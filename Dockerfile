# FROM php:7.4-fpm
# FROM php:7.4-fpm-alpine
FROM php:8.1-fpm
# FROM registry.access.redhat.com/ubi8/php-80

RUN whoami
RUN php -v
RUN cat /etc/*release
RUN cat /etc/passwd
# Arguments defined in docker-compose.yml
# ARG user
# ARG uid


# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq5 \
    libpq-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libonig-dev \
    libxml2-dev \
    libmemcached-dev \
    lua-zlib-dev \
    locales \
    jpegoptim optipng pngquant gifsicle \
    git \
    curl \
    iputils-ping \
    nginx \
    zip \
    # postgresql-dev \
    postgresql postgresql-contrib \
    traceroute \
    unzip

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer
RUN curl -sS https://getcomposer.org/installer​ | php -- --install-dir=/usr/local/bin --filename=composer


# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Copy File PHP.ini
#COPY ./php.ini /usr/local/etc/php/conf.d/
#COPY ./php.ini.php7 /usr/local/etc/php/conf.d/
# RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/conf.d/php.ini"

# Install php extensions
RUN docker-php-ext-install pdo sockets bcmath mbstring pdo_pgsql pgsql exif pcntl gd
# RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql && docker-php-ext-install pgsql pdo_pgsql

# RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
#    install-php-extensions mbstring pdo_mysql exif pcntl gd memcached


# Create system user to run Composer and Artisan Commands
# RUN useradd -G www-data,root -u $uid -d /home/$user $user
#RUN useradd -G www-data,root -u 1000
#RUN useradd -u 1000 -ms /bin/bash -g www www
# Add user for laravel application
#RUN groupadd -g 1000 www
# RUN mkdir -p /home/$user/.composer && chown -R $user:$user /home/$user




# Set working directory
WORKDIR /var/www


# Copy nginx/php/supervisor configs
# RUN cp docker/supervisor.conf /etc/supervisord.conf
# RUN cp docker/php.ini /usr/local/etc/php/conf.d/app.ini
# RUN cp docker/nginx.conf /etc/nginx/sites-enabled/default

# Copy code to /var/www
COPY --chown=www-data:root . /var/www
# COPY . /var/www


# PHP Error Log Files
RUN mkdir /var/log/php
RUN touch /var/log/php/errors.log && chmod 777 /var/log/php/errors.log



# Deployment steps
RUN composer install --optimize-autoloader --no-dev
RUN php artisan key:generate
# RUN php artisan migrate
#try
RUN composer dump-autoload
RUN php artisan config:clear
RUN php artisan config:cache
RUN composer require google/cloud-storage --with-all-dependencies

RUN chown -R www-data /var/www

# RUN chmod +x /var/www/docker/run.sh

USER www-data

CMD php artisan serve --host=0.0.0.0 --port=8080
EXPOSE 8080
