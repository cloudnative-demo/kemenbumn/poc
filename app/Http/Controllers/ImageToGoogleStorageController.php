<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google\Cloud\Storage\StorageClient;


class MyControllerName extends Controller
{
    //
    public function index()
    {
        $googleConfigFile = file_get_contents(config_path('superadmin.json'));
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('googlecloud.storage_bucket');
        $bucket = $storage->bucket($storageBucketName);
        $fileSource = fopen('<<<<your_file_path>>>>', 'r');
        $newFolderName = 'image';
        $googleCloudStoragePath = $newFolderName.'/'.'Video.mov';
        /* Upload a file to the bucket.
        Using Predefined ACLs to manage object permissions, you may
        upload a file and give read access to anyone with the URL.*/
        $bucket->upload($fileSource, [
        // 'predefinedAcl' => 'publicRead',
        'name' => $googleCloudStoragePath
        ]);

        return response()->json([
        "status" => "success",
        "message" => "Image saved successfully ",
        "data" => [
            "url" => url($googleCloudStoragePath),
            "google_storage_url" => 'https://storage.cloud.google.com/'.$storageBucketName.'/'.$googleCloudStoragePath
        ]
        ]);
        // return view("welcome" , []);
    }
}
