<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Images;
use App\Services\GoogleStorageService as GoogleStorageService;

class ImageController extends Controller
{
    private $googleStorageService;
    public function __construct(GoogleStorageService $googleStorageService){
        $this->googleStorageService = $googleStorageService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('imageUpload');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->extension();

        // simpan ke folder /storage/app/public/image
        $request->image->storeAs('images', $imageName);
        // insert ke postgre
        $image = Images::create(['tittle' => $request->tittle, 'description' => $request->description, 'image' => $imageName]);
        /*
            Write Code Here for
            Store $imageName name in DATABASE from HERE
        */

        $result = $this->googleStorageService->uploadFile($imageName);

        $gcp_url = $result['GCP_URL'];

        Images::where('id', $image->id)
            ->update(['gcp_url' => $gcp_url]);

        return back()->with('success','You have successfully upload image')
            ->with('image',$imageName);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
